-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 27, 2020 at 06:59 PM
-- Server version: 10.3.16-MariaDB
-- PHP Version: 7.3.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `restdb`
--

-- --------------------------------------------------------

--
-- Table structure for table `employee`
--

CREATE TABLE `employee` (
  `id` int(11) NOT NULL,
  `age` int(11) NOT NULL,
  `city` varchar(255) DEFAULT NULL,
  `dept` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `phone` varchar(10) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `employee`
--

INSERT INTO `employee` (`id`, `age`, `city`, `dept`, `email`, `name`, `phone`) VALUES
(1, 22, 'Athgarh', 'testing', 'iti@gmail.com', 'itishree mohapatra', '9432579069'),
(2, 23, 'kolkata', 'devlopment', 'sayandeep@gmail.com', 'sayandeep sarkar', '9674198133'),
(3, 23, 'boudh', 'testing', 'santosh@gmail.com', 'santosh kumar parida', '9988776651'),
(4, 34, 'kolkata', 'devlopment', 'riju@gmail.com', 'riju saha', '7788669958'),
(5, 22, 'bihar', 'devlopment', 'akanshya@gmail.com', 'akanshya kumari', '7766554439'),
(6, 30, 'kolkata', 'support', 'arju@gmail.com', 'arju halder', '5566778842'),
(7, 26, 'kolkata', 'testing', 'shuvayan@gmail.com', 'shuvayan mandal', '6677995546'),
(8, 23, 'pune', 'devlopment', 'swati@gmail.com', 'swati bhuyan', '9877777777'),
(9, 27, 'banglore', 'testing', 'subham@gmail.com', 'subham swain', '9567890987'),
(10, 25, 'goa', 'testing', 'smita@gmail.com', 'smita acharya', '7678900987'),
(11, 23, 'cuttack', 'support', 'sunil@gmail.com', 'sunil kumar samal', '8993876543'),
(12, 26, 'bhubaneswar', 'testing', 'surya@gmail.com', 'surya mohapatra', '8876543219'),
(13, 23, 'khordha', 'devlopment', 'manoj@gmail.com', 'manoj dash', '7766889999'),
(14, 30, 'kolkata', 'testing', 'salini@gmail.com', 'salini mohanty', '9876543578');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `active` bit(1) NOT NULL,
  `password` varchar(255) DEFAULT NULL,
  `roles` varchar(255) DEFAULT NULL,
  `user_name` varchar(255) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `active`, `password`, `roles`, `user_name`) VALUES
(100, b'1', 'admin@123', 'ROLE_ADMIN', 'admin@gmail.com'),
(1, b'1', 'iti123', 'ROLE_USER', 'iti@gmail.com'),
(2, b'1', 'sayandeep123', 'ROLE_USER', 'sayandeep@gmail.com'),
(3, b'1', 'santosh123', 'ROLE_USER', 'santosh@gmail.com'),
(4, b'1', 'riju123', 'ROLE_USER', 'riju@gmail.com'),
(5, b'1', 'akanshya123', 'ROLE_USER', 'akanshya@gmail.com'),
(6, b'1', 'arju123', 'ROLE_USER', 'arju@gmail.com'),
(7, b'1', 'shuvayan123', 'ROLE_USER', 'shuvayan@gmail.com');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `employee`
--
ALTER TABLE `employee`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
